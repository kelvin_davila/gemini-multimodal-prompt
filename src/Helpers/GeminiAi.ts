import {GoogleGenerativeAI} from "@google/generative-ai";

export const GEMINI_KEY = "AIzaSyB0ySSi3-yDqS1pVD-n2ea_BX_1de65FGc";
console.log("API: " + GEMINI_KEY);
const genAI = new GoogleGenerativeAI(GEMINI_KEY);

export const runGemini = async (
  prompt = "",
  imageParts: never[]
) => {
  const model = genAI.getGenerativeModel({ model: "gemini-pro-vision" });
  try {
    const result = await model.generateContent([prompt, ...imageParts]);
    const response= result.response;

    return response.text();
  } catch (error: unknown) {
    // @ts-ignore
    return { error: true, message: error.message };
  }

};
