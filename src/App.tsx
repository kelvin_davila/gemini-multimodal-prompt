import { useEffect, useState } from "react";
import { GoogleGenerativeAI } from "@google/generative-ai";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import { FaRegImages } from "react-icons/fa";
import { FaImage } from "react-icons/fa";
import "./App.css";
import SingleComparison from "./Components/SingleComparison";
import MultipleComparisons from "./Components/MultipleComparisons";

function App() {
  const [single, setSingle] = useState(true);

  return (
    <main className="mb-12 w-full p-4 md:px-20">
      <div className="pb-4">
        <h1 className="text-black">Demo Api Gemini</h1>
      </div>

      <section className="flex w-full justify-evenly flex-col md:flex-row gap-4 p2 sm:p-4  ">
        <button
          style={{
            background: single ? "purple" : "#ccc",
            color: single ? "white" : "black"
          }}
          className="w-full transition-all duration-250 ease-in"
          onClick={() => !single && setSingle(true)}
        >
          <FaImage size="1.5rem" className="inline-block mr-2" />
          Uma imagem
        </button>
        <button
          style={{
            background: !single ? "purple" : "#ccc",
            color: !single ? "white" : "black"
          }}
          className="w-full transition-all duration-250 ease-in"
          onClick={() => single && setSingle(!true)}
        >
          <FaRegImages size="1.5rem" className="inline-block mr-2" />
          Comparar imagens
        </button>
      </section>
      {single && <SingleComparison />}
      {!single && <MultipleComparisons />}
      <hr />
    </main>
  );
}

export default App;
