# Gemini Multimodal Chat

Gemini Multimodal prompt é uma aplicação de chat multimodal que utiliza a API Gemini para geração de respostas usando inteligência artificial.

## Como Rodar

Para rodar a aplicação localmente, siga os passos abaixo:

1. Clone este repositório em sua máquina local:

   ```bash
   git clone https://gitlab.com/kelvin_davila/gemini-multimodal-prompt.git
   ```

2. Navegue até o diretório do projeto:

   ```bash
   cd gemini-multimodal-prompt
   ```

3. Instale as dependências do projeto utilizando npm:

   ```bash
   npm install
   ```

4. Após a instalação das dependências, você pode iniciar o servidor de desenvolvimento utilizando o seguinte comando:

   ```bash
   npm run dev
   ```

5. Acesse a aplicação em seu navegador web:

   ```
   http://localhost:5173
   ```

## Contribuição

Contribuições são bem-vindas! Se você encontrar problemas, bugs ou tiver sugestões de melhorias, sinta-se à vontade para abrir uma issue ou enviar um pull request.